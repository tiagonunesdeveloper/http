import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardListaComponent} from './dashboard-lista/dashboard-lista.component';

const routes: Routes = [{path: '', component: DashboardListaComponent}];

@NgModule({imports: [RouterModule.forChild(routes)], exports: [RouterModule]})
export class DashboardRoutingModule {
}
